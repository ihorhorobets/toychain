use log::*;
use sha2::Digest;
pub(crate) fn calculate_hash(
    id: u64,
    timestamp: i64,
    previous_hash: &[u8; 32],
    data: &str,
    nonce: u64,
) -> [u8; 32] {
    let mut hasher = sha2::Sha256::new();
    let data = serde_json::json!({
        "id": id,
        "previous_hash": previous_hash,
        "data": data,
        "timestamp": timestamp,
        "nonce": nonce
    });
    hasher.update(data.to_string().as_bytes());
    hasher
        .finalize()
        .as_slice()
        .try_into()
        .expect("can't convert")
}

pub(crate) const DIFFICULTY_PREFIX: [u8; 2] = [0, 0];

pub(crate) fn mine_block(
    id: u64,
    timestamp: i64,
    previous_hash: &[u8; 32],
    data: &str,
) -> (u64, [u8; 32]) {
    info!("mining block...");
    let mut nonce = 0;

    loop {
        if nonce % 100000 == 0 {
            info!("nonce: {}", nonce);
        }
        let hash = calculate_hash(id, timestamp, previous_hash, data, nonce);
        let binary_hash = hash_to_binary_representation(&hash);
        if hash.starts_with(&DIFFICULTY_PREFIX) {
            info!(
                "mined! nonce: {}, hash: {}, binary hash: {}",
                nonce,
                hex::encode(hash),
                binary_hash
            );
            return (nonce, hash);
        }
        nonce += 1;
    }
}
pub(crate) fn hash_to_binary_representation(hash: &[u8]) -> String {
    let mut res: String = String::default();
    for c in hash {
        res.push_str(&format!("{:b}", c));
    }
    res
}
