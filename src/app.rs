use std::io::Error;

use chrono::Utc;
use log::*;
use tokio::sync::mpsc;

use crate::block::Block;
use crate::network::ChainResponse;
use crate::utils::{calculate_hash, DIFFICULTY_PREFIX};
pub struct App {
    pub blocks: Vec<Block>,
    pub response_sender: mpsc::UnboundedSender<ChainResponse>,
    pub init_sender: mpsc::UnboundedSender<bool>,
}
impl App {
    pub fn new(
        response_sender: mpsc::UnboundedSender<ChainResponse>,
        init_sender: mpsc::UnboundedSender<bool>,
    ) -> Self {
        Self {
            blocks: vec![],
            response_sender,
            init_sender,
        }
    }

    pub fn genesis(&mut self) {
        let genesis_block = Block {
            id: 0,
            timestamp: Utc::now().timestamp(),
            previous_hash: [0; 32],
            data: String::from("genesis!"),
            nonce: 2836,
            hash: [0; 32],
        };
        self.blocks.push(genesis_block);
    }

    pub fn try_add_block(&mut self, block: Block) -> Result<(), Error> {
        if let Some(latest_block) = self.blocks.last() {
            if self.is_block_valid(&block, latest_block) {
                self.blocks.push(block);
                return Ok(());
            }
            Err(Error::new(
                std::io::ErrorKind::InvalidData,
                "could not add block - invalid",
            ))
        } else {
            Err(Error::new(
                std::io::ErrorKind::NotFound,
                "there is at least one block",
            ))
        }
    }

    fn is_block_valid(&self, block: &Block, previous_block: &Block) -> bool {
        dbg!(block);
        dbg!(previous_block);
        if block.previous_hash != previous_block.hash {
            warn!("block with id: {} has wrong previous hash", block.id);
            return false;
        } else if !block.hash.starts_with(&DIFFICULTY_PREFIX) {
            warn!("block with id: {} has invalid difficulty", block.id);
            return false;
        } else if block.id != previous_block.id + 1 {
            warn!(
                "block with id: {} is not the next block after the latest: {}",
                block.id, previous_block.id
            );
            return false;
        } else if calculate_hash(
            block.id,
            block.timestamp,
            &block.previous_hash,
            &block.data,
            block.nonce,
        ) != block.hash
        {
            warn!("block with id: {} has invalid hash", block.id);
            return false;
        }
        true
    }

    fn is_chain_valid(&self, chain: &[Block]) -> bool {
        for i in 1..chain.len() {
            let first = chain.get(i - 1).expect("has to exist");
            let second = chain.get(i).expect("has to exist");
            if !self.is_block_valid(second, first) {
                return false;
            }
        }
        true
    }

    pub fn choose_chain(&mut self, local: Vec<Block>, remote: Vec<Block>) -> Vec<Block> {
        let is_local_valid = self.is_chain_valid(&local);
        let is_remote_valid = self.is_chain_valid(&remote);

        if is_local_valid && is_remote_valid {
            if local.len() >= remote.len() {
                local
            } else {
                remote
            }
        } else if is_remote_valid && !is_local_valid {
            remote
        } else if !is_remote_valid && is_local_valid {
            local
        } else {
            panic!("local and remote chains are both invalid");
        }
    }
}
