use core::time::Duration;
use libp2p::core::upgrade;
use libp2p::futures::StreamExt;
use libp2p::noise::Config as NoiseConfig;
use libp2p::swarm::SwarmEvent;
use libp2p::tcp::tokio::Transport as TokioTcpTransport;
use libp2p::tcp::Config as TcpConfig;
use libp2p::{gossipsub, Transport};
use libp2p::{swarm::SwarmBuilder, Swarm};
use libp2p_mplex::MplexConfig;
use log::*;

use libp2p::mdns::Event as MdnsEvent;
use std::collections::hash_map::DefaultHasher;
use std::error::Error;
use std::hash::{Hash, Hasher};
use tokio::{
    io::{stdin, BufReader},
    select, spawn,
    sync::mpsc,
    time::sleep,
};

mod app;
mod block;
mod network;
mod utils;

use network::MyBehaviourEvent;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::init();

    info!("Peer Id: {}", network::PEER_ID.clone());

    let (response_sender, mut response_rcv) = mpsc::unbounded_channel();
    let (init_sender, mut init_rcv) = mpsc::unbounded_channel();

    let mut app = app::App::new(response_sender, init_sender.clone());

    let auth_keys = network::KEYS.clone();

    let transp = TokioTcpTransport::new(TcpConfig::new().port_reuse(true))
        .upgrade(upgrade::Version::V1)
        .authenticate(NoiseConfig::new(&auth_keys).unwrap())
        .multiplex(MplexConfig::new())
        .boxed();

    let message_id_fn = |message: &gossipsub::Message| {
        let mut s = DefaultHasher::new();
        message.data.hash(&mut s);
        gossipsub::MessageId::from(s.finish().to_string())
    };

    let gossipsub_config = gossipsub::ConfigBuilder::default()
        .heartbeat_interval(Duration::from_secs(10))
        .validation_mode(gossipsub::ValidationMode::None)
        .message_id_fn(message_id_fn)
        .build()
        .expect("Valid config");

    let mut gossipsub = gossipsub::Behaviour::new(
        gossipsub::MessageAuthenticity::Signed(auth_keys.clone()),
        gossipsub_config,
    )
    .expect("Correct configuration");

    let block_topic = gossipsub::IdentTopic::new(network::BLOCK_TOPIC.clone());
    gossipsub.subscribe(&block_topic).unwrap();
    let behaviour = network::AppBehaviour::new(gossipsub).await;

    let mut swarm = SwarmBuilder::with_tokio_executor(transp, behaviour, *network::PEER_ID).build();

    let stdin = stdin();
    let mut stdin = BufReader::new(stdin);

    Swarm::listen_on(
        &mut swarm,
        "/ip4/0.0.0.0/tcp/0"
            .parse()
            .expect("can get a local socket"),
    )
    .expect("swarm can be started");

    spawn(async move {
        sleep(Duration::new(1, 0)).await;
        info!("sending init event");
        init_sender.send(true).expect("can send init event");
    });
    use tokio::io::AsyncBufReadExt;
    let buf = &mut String::new();
    swarm.select_next_some().await;
    loop {
        let evt = {
            select! {
                _line = stdin.read_line(buf)  => Some(network::EventType::Input(buf.trim().to_string())),

                response = response_rcv.recv() => {
                    Some(network::EventType::LocalChainResponse(response.expect("response exists")))
                },
                _init = init_rcv.recv() => {
                    Some(network::EventType::Init)
                }
                event = swarm.next() => {
                    if let SwarmEvent::Behaviour(behaviour_event) = event.unwrap() {
                            match behaviour_event {
                                MyBehaviourEvent::Gossipsub(gossip_event) => {
                                    info!("Gossip Event: {:?}", gossip_event);
                                    network::inject_floodsub_event(&gossip_event, &mut app);

                                },
                                MyBehaviourEvent::Mdns(mdns_event) => {
                                    info!("Mdns Event: {:?}", mdns_event);
                                    if let MdnsEvent::Discovered(list) = mdns_event {
                                        for (peer_id, multiaddr) in list {
                                            info!("Discovered {:?} {:?}", peer_id, multiaddr);
                                            swarm.behaviour_mut().gossipsub.add_explicit_peer(&peer_id);
                                        }
                                    }
                                },
                                MyBehaviourEvent::Floodsub(floodsub_event) => {
                                    info!("Floodsub Event: {:?}", floodsub_event);
                                },
                            };
                        }
                    None
                },
            }
        };

        if let Some(event) = evt {
            match event {
                network::EventType::Init => {
                    let peers = network::get_list_peers(&swarm);

                    app.genesis();

                    info!("connected nodes: {}", peers.len());
                    if !peers.is_empty() {
                        let req = network::LocalChainRequest {
                            from_peer_id: peers
                                .iter()
                                .last()
                                .expect("at least one peer")
                                .to_string(),
                        };

                        let json = serde_json::to_string(&req).expect("can jsonify request");
                        let _ = swarm.behaviour_mut().gossipsub.publish(
                            gossipsub::IdentTopic::new(network::CHAIN_TOPIC.clone()).hash(),
                            json.as_bytes(),
                        );
                    }
                }
                network::EventType::LocalChainResponse(resp) => {
                    let json = serde_json::to_string(&resp).expect("can jsonify response");
                    let _ = swarm.behaviour_mut().gossipsub.publish(
                        gossipsub::IdentTopic::new(network::CHAIN_TOPIC.clone()).hash(),
                        json.as_bytes(),
                    );
                }
                network::EventType::Input(line) => match line.as_str() {
                    "ls p" => network::handle_print_peers(&swarm),
                    cmd if cmd.starts_with("ls c") => network::handle_print_chain(&app),
                    cmd if cmd.starts_with("create b") => {
                        network::handle_create_block(cmd, &mut swarm, &mut app)
                    }
                    _ => error!("unknown command {}!", line),
                },
            }
        }
    }
}
