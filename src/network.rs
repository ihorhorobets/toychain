use std::collections::HashSet;

use libp2p::floodsub::FloodsubEvent;
use libp2p::gossipsub;
use libp2p::identity::{self, Keypair};
use libp2p::mdns::tokio::Behaviour;
use libp2p::mdns::Event as MdnsEvent;
use libp2p::swarm::NetworkBehaviour;

use libp2p::{PeerId, Swarm};
use log::*;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};

use crate::app::App;
use crate::block::Block;

pub static KEYS: Lazy<Keypair> = Lazy::new(identity::Keypair::generate_ed25519);
pub static PEER_ID: Lazy<PeerId> = Lazy::new(|| PeerId::from(KEYS.public()));
pub static CHAIN_TOPIC: &str = "chains";
pub static BLOCK_TOPIC: &str = "blocks";

#[derive(Debug, Serialize, Deserialize)]
pub struct ChainResponse {
    pub blocks: Vec<Block>,
    pub receiver: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LocalChainRequest {
    pub from_peer_id: String,
}

#[derive(Debug)]
pub enum EventType {
    LocalChainResponse(ChainResponse),
    Input(String),
    Init,
}

#[derive(Debug)]
pub enum MyBehaviourEvent {
    Gossipsub(gossipsub::Event),
    Mdns(MdnsEvent),
    Floodsub(FloodsubEvent),
}

impl From<gossipsub::Event> for MyBehaviourEvent {
    fn from(event: gossipsub::Event) -> Self {
        MyBehaviourEvent::Gossipsub(event)
    }
}

impl From<MdnsEvent> for MyBehaviourEvent {
    fn from(event: MdnsEvent) -> Self {
        MyBehaviourEvent::Mdns(event)
    }
}
impl From<FloodsubEvent> for MyBehaviourEvent {
    fn from(value: FloodsubEvent) -> Self {
        MyBehaviourEvent::Floodsub(value)
    }
}

#[derive(NetworkBehaviour)]
#[behaviour(to_swarm = "MyBehaviourEvent")]
pub struct AppBehaviour {
    pub gossipsub: gossipsub::Behaviour,
    pub mdns: Behaviour,
}

impl AppBehaviour {
    pub async fn new(gossipsub: gossipsub::Behaviour) -> Self {
        Self {
            gossipsub,
            mdns: Behaviour::new(Default::default(), *PEER_ID).expect("can't create behaviour"),
        }
    }
}
pub fn inject_floodsub_event(event: &gossipsub::Event, app: &mut App) {
    if let gossipsub::Event::Message { message, .. } = event {
        let msg = message;
        if let Ok(resp) = serde_json::from_slice::<ChainResponse>(&msg.data) {
            if resp.receiver == PEER_ID.to_string() {
                info!("Response from {}:", msg.source.unwrap());
                resp.blocks.iter().for_each(|r| info!("{:?}", r));

                app.blocks = app.choose_chain(app.blocks.clone(), resp.blocks);
            }
        } else if let Ok(resp) = serde_json::from_slice::<LocalChainRequest>(&msg.data) {
            info!("sending local chain to {}", msg.source.unwrap().to_string());
            let peer_id = resp.from_peer_id;
            if PEER_ID.to_string() == peer_id {
                if let Err(e) = app.response_sender.send(ChainResponse {
                    blocks: app.blocks.clone(),
                    receiver: msg.source.unwrap().to_string(),
                }) {
                    error!("error sending response via channel, {}", e);
                }
            }
        } else if let Ok(block) = serde_json::from_slice::<Block>(&msg.data) {
            info!("received new block from {:?}", msg.source);
            let _ = app.try_add_block(block);
        }
    }
}
pub fn get_list_peers(swarm: &Swarm<AppBehaviour>) -> Vec<String> {
    info!("Discovered Peers:");
    let nodes = swarm.behaviour().mdns.discovered_nodes();
    let mut unique_peers = HashSet::new();
    for peer in nodes {
        unique_peers.insert(peer);
    }
    unique_peers.iter().map(|p| p.to_string()).collect()
}

pub fn handle_print_peers(swarm: &Swarm<AppBehaviour>) {
    let peers = get_list_peers(swarm);
    peers.iter().for_each(|p| info!("{}", p));
}

pub fn handle_print_chain(app: &App) {
    info!("Local Blockchain:");
    let pretty_json = serde_json::to_string_pretty(&app.blocks).expect("can jsonify blocks");
    info!("{}", pretty_json);
}

pub fn handle_create_block(cmd: &str, swarm: &mut Swarm<AppBehaviour>, app: &mut App) {
    if let Some(data) = cmd.strip_prefix("create b") {
        let behaviour = swarm.behaviour_mut();
        let latest_block = app.blocks.last().expect("there is at least one block");
        let block = Block::new(latest_block.id + 1, latest_block.hash, data.to_owned());
        let json = serde_json::to_string(&block).expect("can jsonify request");
        app.blocks.push(block);
        info!("broadcasting new block");
        behaviour
            .gossipsub
            .publish(
                gossipsub::IdentTopic::new(BLOCK_TOPIC.clone()),
                json.as_bytes(),
            )
            .unwrap();
    }
}
