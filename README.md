# toychain

A toy blockchain implementation in Rust.

## Usage

```
RUST_LOG=info cargo run
create b block_data
ps c
```

![screenshot](./images/screen.png)

## Todo

- [ ] implement smartcontracts
